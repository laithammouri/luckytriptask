<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'v1'
], function () {
    Route::get('/airports/{id?}', 'AirportController@index');
    Route::post('/airports', 'AirportController@create');
    Route::delete('/airports/{id?}', 'AirportController@destroy');
    Route::put('/airports', 'AirportController@update');
    Route::post('/airports/search', 'AirportController@search');
});