<?php


namespace App\Repositories;


use App\Interfaces\AirportRepositoryInterface;
use App\Models\Airport;

class AirportRepository extends BaseRepository implements AirportRepositoryInterface
{
    protected $model;

    public function __construct(Airport $airport)
    {
        $this->model = $airport;
    }


    public function addAirport(Array $airportData): ?array
    {
        $model = $this->model::create($airportData);

        if (!$model) {
            return null;
        }

        return $model->toArray();
    }

    public function deleteAirport(int $id): bool
    {
        return $this->model::where('id', $id)->delete();
    }

    public function getAirportById(int $id): array
    {
        $model = $this->model::where('id', $id)->first();
        return !is_null($model) ? $model->toArray() : [];
    }

    public function updateAirport(array $airportData): bool
    {
        return $this->model::where('id', $airportData['id'])->update($airportData);
    }

    public function searchByName(string $airportName) : ?array
    {
        return $this->model::where('name', 'like', '%' . $airportName . '%')->limit(5)->get()->toArray();
    }

    public function searchByCoordinates(array $coordinates): ?array
    {
        $airports = $this->model::all()->toArray();

        $closestAirports = [];

        foreach ($airports as $key => $airport) {
            $a = $coordinates['latitude'] - $airport['latitude'];
            $b = $coordinates['longitude'] - $airport['longitude'];
            $distance = sqrt(($a ** 2) + ($b ** 2));
            $closestAirports[$key] = ['airport' => $airport, 'distance' => $distance];
        }

        usort($closestAirports, function ($airport1, $airport2) {
            return $airport1['distance'] <=> $airport2['distance'];
        });

        return array_slice($closestAirports, 0, 5);
    }

}