<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetAirportRequest;
use App\Interfaces\AirportRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Validator;

class AirportController extends Controller
{
    private $airportRepository;
    public function __construct(AirportRepositoryInterface $airportRepository)
    {
        $this->airportRepository = $airportRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetAirportRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if(empty($id)) {
            return response()->json([
                'message' => 'id is required'
            ],Response::HTTP_UNAUTHORIZED);
        }

        $airport = $this->airportRepository->getAirportById($id);

        if(empty($airport)) {
            return response()->json([
                'data' => $airport,
            ],Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'data' => $airport,
        ],Response::HTTP_OK);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:60',
            'iata_code' => 'required|string|min:2|max:6',
            'latitude' => 'required|between:-99.99,99.99',
            'longitude' => 'required|between:-99.99,99.99',
        ]);

        if($validator->fails()) {
            return response()->json([
                'data' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        $result = $this->airportRepository->addAirport($request->all());

        if(!$result) {
            return response()->json([
                'message' => 'failed to create record'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'data' => $result
        ], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(empty($request->all())) {
            return response()->json([
                'message' => 'no fields to update'
            ], Response::HTTP_BAD_REQUEST);
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:airports,id',
            'name' => 'string|min:3|max:60',
            'iata_code' => 'string|min:2|max:6',
            'latitude' => 'between:-99.99,99.99',
            'longitude' => 'between:-99.99,99.99',
        ]);

        if($validator->fails()) {
            return response()->json([
                'data' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        $result = $this->airportRepository->updateAirport($request->all());

        if(!$result) {
            return response()->json([
                'message' => 'failed to update record'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'message' => 'updated'
        ], Response::HTTP_OK);
    }

    /**
     * @param null $id
     * @return JsonResponse
     */
    public function destroy($id = null)
    {
        if(empty($id)) {
            return response()->json([
                'message' => 'id is required'
            ],Response::HTTP_UNAUTHORIZED);
        }

        $airport = $this->airportRepository->getAirportById($id);

        if(empty($airport)) {
            return response()->json([
                'data' => $airport,
            ],Response::HTTP_NOT_FOUND);
        }

        $result = $this->airportRepository->deleteAirport($id);

        if(!$result) {
            return response()->json([
                'message' => 'failed to delete record'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
        ],Response::HTTP_NO_CONTENT);
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required_without:coordinates',
            'coordinates' => 'required_without:name|array',
            'coordinates.longitude' => 'required_without:name|between:-99.99,99.99',
            'coordinates.latitude' => 'required_without:name|between:-99.99,99.99',
        ]);


        if($validator->fails()) {
            return response()->json([
                'data' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        $airports = [];

        if (array_key_exists('name', $request->all())) {
            $airports = $this->airportRepository->searchByName($request->get('name'));
        } elseif(array_key_exists('coordinates', $request->all())) {
            $airports = $this->airportRepository->searchByCoordinates(['longitude' => '-54.3123', 'latitude' => '54.3123']);
        }

        if (empty($airports)) {
            return response()->json([
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'data' => $airports,
        ], Response::HTTP_OK);


    }
}
