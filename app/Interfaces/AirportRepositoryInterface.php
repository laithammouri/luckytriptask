<?php


namespace App\Interfaces;

interface AirportRepositoryInterface
{

    public function getAirportById(int $id): array;

    public function addAirport(Array $airportData): ?array;

    public function updateAirport(array $airportData): bool;

    public function deleteAirport(int $id): bool;

    public function searchByName(string $airportName) : ?array;

    public function searchByCoordinates(array $coordinates) : ?array;
}