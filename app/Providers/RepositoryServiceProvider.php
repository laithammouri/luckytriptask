<?php
namespace App\Providers;
use App\Repositories\AirportRepository;
use App\Interfaces\AirportRepositoryInterface;
use Illuminate\Support\ServiceProvider;
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            AirportRepositoryInterface::class,
            AirportRepository::class
        );
    }
}