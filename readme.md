# Project Title

LuckyTrip Task

## Description

This mini project consists of 5 RESTFUL CRUD operations on airports table. 

### Prerequisites

* PHP 7.2 +
* Laravel 5.6.*

## Running the Application

* Clone the project
* cd into the project folder
* In your terminal run ```composer install```
* Change the db configs in the ```.env```

* In your terminal run ```mysql -u your-user-name -p your-password your-database-name < airports.sql``` to create the table.
* In your terminal run ```mysql -u your-user-name -p your-password your-database-name < airports-seeder.sql``` to seed the table with some data.
* In your terminal run ```php artisan serve``` to start up the server.
* Check the implemented that are prefixed with ```http://your-host:port/api/v1/[resource]``` and enjoy!

## Authors

* **Laith N. Al-Ammouri**
